#### External Links

- [Social Cooling](https://www.socialcooling.com/) - Big Data's unintended side effect

- [An open letter to Government of India to enforce Free Software](https://wiki.fsci.org.in/index.php/An_Open_Letter_to_Ministry_of_Communications_and_Information_Technology_India).

- [How Technology is Hijacking Your Mind](https://medium.com/thrive-global/how-technology-hijacks-peoples-minds-from-a-magician-and-google-s-design-ethicist-56d62ef5edf3)

- [PrivacyTools](https://privacytools.io/classic/) ( [Onion Link](http://www.privacy2zbidut4m4jyj3ksdqidzkw3uoip2vhvhbvwxbqux5xy5obyd.onion/classic/) ) - Encryption and tools to protect against global mass surveillance. 

- The [Eternal Value of Privacy](https://www.schneier.com/essays/archives/2006/05/the_eternal_value_of.html).

- A compilation of reasons not to use services/products by [Google](https://stallman.org/google.html), [Facebook](https://stallman.org/facebook.html), [Amazon](https://stallman.org/amazon.html) and [some other tech companies](https://stallman.org/). 

- [RSS Feeds for arbitrary websites](https://feed-me-up-scotty.vincenttunru.com/).

- [Common Myths about Private Browsing](https://support.mozilla.org/en-US/kb/common-myths-about-private-browsing).

- [The Danger of Software Patents](https://www.gnu.org/philosophy/danger-of-software-patents.en.html).

- [Anti-DRM campaign](https://www.defectivebydesign.org/).

- [Freeing the Mind : Free Software and the Death of Proprietary Culture](http://old.law.columbia.edu/publications/maine-speech.html) by Eben Moglen.

- [Why Data Mining Won't Stop Terror](https://www.schneier.com/essays/archives/2005/03/why_data_mining_wont.html)

