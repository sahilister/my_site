Link Policy: I won't give links to YouTube, Twitter, Instagram or any other service which tracks people. Most of the following talks are uploaded on free-software powered services like Peertube or Cryptpad. If I want to share a talk being uploaded to YouTube, I will link to the talk on [Invidious](https://github.com/iv-org/invidious). 

#### List of talks I gave:

- 23-January-2021: [An Introduction to free software communication tools](https://peertube.debian.social/videos/watch/2b21c084-9522-4c8c-98c7-985c06d3b9d5) (in Hindi). Event: [MiniDebConf India 2021](https://in2021.mini.debconf.org/). 

If you would like to check out other videos of MiniDebConf India 2021, then please visit [this URL](https://peertube.debian.social/video-channels/mdcoin2021/videos).

- 23-January-2021: [BoF: Digital privacy in Indian context and some Free software](https://peertube.debian.social/videos/watch/417bd62c-cce1-494a-b96c-b0e39741cb09) (in Hindi). Event: [MiniDebConf India 2021](https://in2021.mini.debconf.org/). 

#### Talks by other people:

- [Freedom in the 'cloud'](https://cryptpad.fr/slide/#/2/slide/view/k-RoDDQMrOsouAswzBYUfiaVa-CQWEzSRoTyxlJwiRc/) by [Pirate Praveen](https://social.masto.host/@praveen).

- Richard Stallman's TEDx video: [Introduction to Free Software and the Liberation of Cyberspace](https://audio-video.gnu.org/video/TEDxGE2014_Stallman05_LQ.webm).
