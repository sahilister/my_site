---
title: "Home"
---

Welcome. You can check about me [in this section](https://ravidwivedi.in/about) and check out my posts [here](https://ravidwivedi.in/posts)! In my blog, I aim to make digital privacy accessible to common people. 

If you have any comments/suggestions on the website, please [write to me](https://ravidwivedi.in/contact/).
