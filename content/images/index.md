<!---
Mistake: In the index.md, I wrote ![Encryption](/static/shredder.png)
Correction: Should not use static Dir name in linking content. All content of static folder will be placed in root of the website during build. So, static/images/shredder.png becomes /images/shredder.png 
-->
<figure>
<img
     src="/images/shredder.png"
     alt="Drawing: Paper is passed through a device which shreds it into fragments. The fragments travel some distance and then are re-assembled by another device">

<figcaption> End-to-end encryption  </figcaption>
</figure>

<br>

<figure>
  <img src="/images/proprietary.jpg" alt="Cartoon: A user is being walked on a leash. The leash is held by proprietary software." >
  <figcaption> Proprietary Software controls the user </figcaption>
</figure>

<br>
