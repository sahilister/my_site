#### Contact

The best way to contact me is via email.

- Email: ravi at ravidwivedi dot in .

Please don't forget to encrypt your mails with [my GPG keys](/files/ravidwivedi.asc). 
(Keys created on: 13-June-2021).

If you aren't encrypting your emails already, [try pep](https://www.pep.security/en/), which is a [free/libre software](https://www.gnu.org/philosophy/free-software-even-more-important.html), very easy to use and your mails will be encrypted by default. A guide for encryption using pep is [here](https://fsci.in/blog/pep-guide/).

Further, I recommend you to read [useplaintext.email](https://useplaintext.email/) which explains why you should write emails in plaintext and not in HTML.

- My XMPP Address is `ravi@poddery.com`. Examples of XMPP apps are: [Quicksy](quicksy.im) or [Conversations](conversations.im). A list of XMPP clients are [here](https://xmpp.org/software/clients.html).

You can verify my OMEMO fingerprints from here:

`6baf8edb d556dd27 6e20a902 
96d5ed31 83629e61 7084120e 
71f82b0a 894c8f77`

- You can contact me using [Briar](https://briarproject.org/).

My Briar ID is:

`briar://aaao5mdon4l6sfmvpipjy4ysz7niotbevd2niixsa7x74rmsglubm`

[Check here](https://briarproject.org/manual/) for manual on how to add contacts on Briar.

- [Tox](https://tox.chat/): My Tox ID is 

`3271BF4867C72A1EBE6B37FB59C53491914FFF789736539F3282AC10EB484305F8F04C5D0CC8`

In your Tox app, go to 'Add Contact', enter my Tox ID and click 'Add'. A friend request will be sent to me. 

I have tested different Tox apps for Android, and TRIfA (https://github.com/zoff99/ToxAndroidRefImpl) seems to work well for me. It is available on F-Droid [here](https://f-droid.org/en/packages/com.zoffcc.applications.trifa/).

- [Jami](https://jami.net/): If Jami works for you, my username is `ravi1` . 

<br>

Note: I do not use any [nonfree/proprietary software(apps)](https://gnu.org/malware) like [WhatsApp](https://fsf.org.in/article/better-than-whatsapp), [Zoom, Google Meet](https://fsf.org.in/article/better-than-zoom) to communicate. I only use free software (like [Jitsi](https://jitsi.org/), [Bigbluebutton](https://directory.fsf.org/wiki/BigBlueButton),[Conversations](https://conversations.im/)) where 'free' [does not refer to price](https://play.google.com/store/apps/details?id=eu.siacs.conversations&referrer=utm_source%3Dwebsite), it refers to freedom. For details about free software, please check [this](https://www.gnu.org/philosophy/free-sw.html). 
