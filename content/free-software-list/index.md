---
title: "List of freedom-respecting software"
---
[Free software](https://www.gnu.org/philosophy/free-software-even-more-important.html) means that the users have the freedom to run, copy, distribute, study, change and improve the software. Thus, “free software” is a matter of liberty, not price. I maintain this list to raise awareness about the free software replacements of the commonly used proprietary tools. Whenever the word 'free' is mentioned, I mean freedom-respecting in the sense we just defined. The term ['open-source' is never used](https://www.gnu.org/philosophy/open-source-misses-the-point.html).

If you are using Android, then I would like to suggest you to download [F-Droid](f-droid.org) which is a repository of free software and an alternative to Google Play Store. F-Droid can be used to explore free software as well by using its search function. 

For the software which require network services to connect for their function(like chatting apps), I have only suggested the decentralized and federated (if applicable, for example, search engines can be decentralized but the definition of federation does not apply) ones. 

Mostly, I have copied the description of these software from the Git repository of the project or from the project's official site.

Please help me in improving this list by sending your suggestions/comments about this list to ravi at ravidwivedi dot in .

Here is the list: 
### Operating System

**GNU/Linux distros** : Some of the popular GNU/Linux distros are [Debian](https://debian.org), [Ubuntu](https://ubuntu.com/), [Fedora](https://getfedora.org/). Sorry, if I did not list your favorite distro. 

Usually, people refer to these operating systems as 'Linux', but this is incorrect as Linux is only the kernel of the operating system, I refer to these distros as GNU/Linux. For a detailed argument, check [here](https://www.gnu.org/gnu/why-gnu-linux.html).

### Web Browser

- [Firefox browser](https://www.mozilla.org/en-US/firefox/new/) :

- [Tor Browser](https://www.torproject.org/) ( [Onion Link](http://2gzyxa5ihm7nsggfxnu52rck2vv4rvmdlkiu3zzui5du4xyclen53wid.onion/) ) : I recommend using Tor Browser for online anonymity. It is a modified version of Firefox ESR, which comes with pre-installed privacy add-ons, encryption, and an advanced proxy.

Check description [here](https://www.torproject.org/about/history/).

- [IceCat Browser](https://www.gnu.org/software/gnuzilla/)

- [Lynx Browser](http://lynx.browser.org/) : Command line browser.


### Webpage Downloader

- [GNU Wget](https://www.gnu.org/software/wget/) : GNU Wget is a free software package for retrieving files using HTTP, HTTPS, FTP and FTPS, the most widely used Internet protocols. It is a non-interactive commandline tool, so it may easily be called from scripts, cron jobs, terminals without X-Windows support, etc. 

Just run the command `wget URL` in the terminal and it will fetch the webpage at the URL and download it in your computer. 

### Text Editor

- [GNU Emacs](https://www.gnu.org/software/emacs/)

- [Vim](https://www.vim.org/) : A command line text editor. Description [here](https://github.com/vim/vim#what-is-vim). 

(This file is made in Vim Text Editor)

### Office Suite

- [LibreOffice](https://www.libreoffice.org/): It is an alternative to Microsoft Office.

### Collaborative Editing

- [Etherpad](https://etherpad.org/) : (Alternative to: Google Docs) Etherpad allows you to edit documents collaboratively in real-time. Write articles, press releases, to-do lists, etc. together with your friends, fellow students or colleagues, all working on the same document at the same time. 

Check out the description of Etherpad [here](https://github.com/ether/etherpad-lite#about).

List of [Etherpad instances](https://github.com/ether/etherpad-lite/wiki/Sites-that-run-Etherpad).

- [Cryptpad](https://cryptpad.fr) (Alternative of: Google Docs)  : CryptPad is a collaboration suite that is end-to-end-encrypted and free software. It is built to enable collaboration, synchronizing changes to documents in real time. Because all data is encrypted, the service and its administrators have no way of seeing the content being edited and stored.
Check description [here](https://github.com/xwiki-labs/cryptpad#cryptpad).

- [Private Bin](https://github.com/PrivateBin/PrivateBin) : PrivateBin is a minimalist and freedom-respecting pastebin that has zero knowledge of pasted data. Everything is encrypted and decrypted in-browser using 256bit AES in Galois Counter mode.

An example instance is [here](https://bin.nixnet.services).


### Keyboard

- [Indic Keyboard](https://indickeyboard.org/) : Indic keyboard is a [privacy-respecting](https://indickeyboard.org/faq.html#how-is-it-privacy-aware) keyboard for Android and [supports typing in Indian languages](https://indickeyboard.org/faq.html#what-all-languages-are-supported). 

### Chatting 

Signal and Telegram are not listed here because they are centralized messengers.

All the apps in this list, except Quicksy, do not ask for phone number or email or any personal details as mandatory to register. Also, all the apps listed here encrypt messages by default. 

![Encryption](/images/shredder.png)
<figcaption> Image: End-to-end encryption </figcaption>

I have written an article on how to choose a privacy-respecting chatting app,you can [read here](https://ravidwivedi.in/posts/chatting-apps/).

#### Federated apps

- [Quicksy](quicksy.im) - Federates with [XMPP](xmpp.org) and can be registered using a phone number, so it is convenient as well.

- [Conversations](conversations.im) - XMPP app. Can be used to contact Quicksy users. Does not ask for any personal details like phone number or email.

List of XMPP apps is [here](https://xmpp.org/software/clients.html).

- [Element](element.io) - A [Matrix](https://matrix.org) app.

List of Matrix apps is [here](https://matrix.org/clients/).


- [Silence](silence.im) - Sends SMS in encrypted form if both the users have Silence App. It can send and receive normal SMS messages from users who are not using the Silence app but they will be unencrypted.

- [Delta Chat](delta.chat) - Sends emails as chat if both the users are using Delta Chat. Needs login using email. Sends normal email to other contacts who are not using Delta Chat. 

#### Peer-to-peer apps

Users exchanging messages need to be online at the same time. You can run the app in the background mode so that app does not disconnect from the internet.  

- [Briar](https://briarproject.org/) : Briar is a peer-to-peer encrypted messaging app for Android and doesn’t rely on a central server - messages are synchronized directly between the users’ devices. If the internet’s down, Briar can sync via Bluetooth or Wi-Fi, keeping the information flowing in a crisis. If the internet’s up, Briar can sync via the Tor network, protecting users and their relationships from surveillance. Check [here](https://briarproject.org/how-it-works/) for description.

- [GNU Jami](https://jami.net/) : Jami is a distributed peer-to-peer and SIP-compatible [softphone](https://en.wikipedia.org/wiki/Softphone) and instant messenger and it is availble for GNU/Linux, macOS, Windows, Android and iOS.

- [Tox](https://tox.chat/) - A list of apps that can communicate using Tox protocol is [here](https://wiki.tox.chat/clients). Neither the Tox protocol nor the implementation have undergone peer review, and its exact security properties and network behaviour are not well-understood, yet. [Toktok project](https://toktok.ltd/) is actively working on improving that situation.

There are other chatting apps, I haven't tried-- [Session](https://getsession.org/), [Status](https://status.im/), [Threema](https://threema.ch/), 

There is another protocol named IRC. I am still not well-versed in IRC. So I do not have any suggestions for that. 

### Video Meeting

- Jitsi Meet : A list of Jitsi instances is [here](https://jitsi.github.io/handbook/docs/community/community-instances). It works like this: Go to a Jitsi Meet instance, for example, [meet.fsci.in](https://meet.fsci.in) and type in the 'Room Name' and click on 'Start Meeting', share the link with the people whom you would like to invite, and they just need to visit the link from a browser to join the meeting. You can also download Jitsi Android or iOS app to create/join the meeting. 

- [BigBluebutton](https://github.com/bigbluebutton/bigbluebutton#bigbluebutton) : BigBlueButton is a free software web conferencing system.

BigBlueButton supports real-time sharing of audio, video, slides (with whiteboard controls), chat, and the screen. Instructors can engage remote students with polling, emojis, multi-user whiteboard, and breakout rooms.

Presenters can record and playback content for later sharing with others.

BigBluebutton is designed for online learning (though it can be used for many other applications). The educational use cases for BigBlueButton are

   -  Online tutoring (one-to-one)
   -  Flipped classrooms (recording content ahead of your session)
   -  Group collaboration (many-to-many)
   -  Online classes (one-to-many)


List of BigBlueButton instances is [here](https://wiki.chatons.org/doku.php/la_visio-conference_avec_big_blue_button). 

Example: Choose a BigBluebutton instance, like [meet.nixnet.services](https://meet.nixnet.services). Sign up for an account. Once you are logged in, you can create rooms and share the links with attendees. Joining a BigBlueButton meeting is as easy as clicking a link and loading it in your browser (Recent versions of Chromium and Firefox) either on your laptop/desktop or on your mobile.

Chiguru offers paid plans for [Online classrooms](https://classmeet.chiguru.tech/) sevice based on BigBlueButton. 

- [MiroTalk](https://mirotalk.herokuapp.com/): Mirotalk is a peer-to-peer video calling software with tons of [features](https://github.com/miroslavpejic85/mirotalk#features). With Mirotalk, you can record meetings, share files, write on whiteboard with full privacy.  

- [GNU Jami](jami.net): I myself haven't tried it for video calling.

There are others I haven't tried myself: [Mumble](https://www.mumble.info/), [Linphone](https://www.linphone.org/).


### Email Clients

Email clients are the software in which you can login using your email and password and then check emails from the app. One advantage is doing this is that a lot of email clients support gpg encryption while usually webmails do not. All the listed apps support gpg encryption. 

 - [pep](pep.security) - Encrypts emails automatically when both users use pep. Encryption means that only the sender and the recipient of the mail can read its contents(body and subject), the mail provider cannot read the encrypted mails. Although, the mail provider has access to the metadata-- the mail address you contacted with and the time of all the incoming and outgoing emails.Currently, the Thunderbird add-on doesn't work well as it disables OpenPGP in Thunderbird. pep has apps for Android, iOS, and plugins for Thunderbird and Outlook. 

 is highly recommended by me! 

- [Thunderbird](thunderbird.net) : Thunderbird is a freedom-respecting email, newsfeed, chat, and calendaring client, that’s easy to set up and customize. One of the core principles of Thunderbird is the use and promotion of open standards.

It also [supports OpenPGP](https://support.mozilla.org/en-US/kb/openpgp-thunderbird-howto-and-faq) and can be used for encrypting emails with gpg.

- [K9 Mail](https://k9mail.app/) :K-9 Mail is a free software (freedom-respecting) email client for Android. It can encrypt emails with OpenPGP using the freedom-respecting Android app  [OpenKeychain](https://openkeychain.org/). Check [this link](https://github.com/k9mail/k-9#k-9-mail) for more details. 


### Downloading YouTube videos

- [youTube-dl](https://github.com/ytdl-org/youtube-dl) : youtube-dl is a command-line program to download videos from YouTube.com and it [supports a lot of other sites](https://ytdl-org.github.io/youtube-dl/supportedsites.html). It is very powerful and you can download videos, full playlists, even full channels by using a single command. It can also extract audio and download the audio files from YouTube and other audio sites. Check the official description [here](https://github.com/ytdl-org/youtube-dl#description). 

- [Newpipe](https://newpipe.net/)

NewPipe is an alternative front-end of YouTube. NewPipe does not use any Google framework libraries, nor the YouTube API. Websites are only parsed to fetch required info, so this app can be used on devices without Google services installed. Also, you don't need a YouTube account to use NewPipe, which is [copylefted](https://www.gnu.org/licenses/copyleft.en.html) libre software. NewPipe is a privacy-respecting way of watching YouTube videos.

Keep in mind that your IP address is exposed to YouTube.



### Front-ends

These are front-ends for accessing some websites which track you but the front-end are free and respect privacy. Use [Privacy Redirect plugin](https://addons.mozilla.org/en-US/firefox/addon/privacy-redirect/) in your browser to automatically redirect to these front-ends. 

- [Invidious](https://github.com/iv-org/invidious): Invidious is a privacy-respcting, free software, alternative front-end to YouTube without any ads or tracking. You can visit any of these public[instances](https://github.com/iv-org/documentation/blob/master/Invidious-Instances.md#list-of-public-invidious-instances-sorted-from-oldest-to-newest) in your web browser and watch YouTube videos on Invidious. 

- [Piped](https://github.com/TeamPiped/Piped) : You can watch YouTube videos using Piped by visiting [this URL](https://piped.kavin.rocks/) in any browser. Piped does not expose your IP address to YouTube. Official description [here](https://github.com/TeamPiped/Piped#piped).

- [Newpipe](https://newpipe.net/) : NewPipe is an alternative front-end of YouTube. NewPipe does not use any Google framework libraries, nor the YouTube API. Websites are only parsed to fetch required info, so this app can be used on devices without Google services installed. Also, you don't need a YouTube account to use NewPipe, which is [copylefted](https://www.gnu.org/licenses/copyleft.en.html) libre software. NewPipe is a privacy-respecting way of watching YouTube videos.

- [CloudTube](https://tube.cadence.moe/) : An alternative freedom-respecting frontend to YouTube. Check description [here](https://sr.ht/~cadence/tube/).

- [Nitter](https://github.com/zedeus/nitter/wiki/Instances) : Privacy-respecting alternative front-end for Twitter. There is no tracking, javascript or ads. You can view profiles, tweets, videos, stats, etc. Also supports RSS feeds. As an example, [visit NY Times official Twitter page using Nitter](https://nitter.nixnet.services/nytimes). 

- [Fritter](https://github.com/jonjomckay/fritter) : A privacy-friendly Twitter frontend for Android. This app is available on F-Droid [here](https://f-droid.org/packages/com.jonjomckay.fritter/).

- [Bibliogram](https://git.sr.ht/~cadence/bibliogram-docs/tree/master/docs/Instances.md) : Bibliogram is a website that takes data from Instagram's public profile views and puts it into a friendlier page that loads faster, gives downloadable images, eliminates ads, generates RSS feeds, and doesn't urge you to sign up. [See an example](https://bibliogram.art/#featured-profiles).

- [Libreddit](https://libredd.it/): An alternative privacy-respecting front-end to Reddit.

- [Teddit](teddit.net): Privacy respecting alternative front-end to Reddit. Check the description [here](https://teddit.net/about). 

### Music/Video Player

- [VLC Media Player](https://www.videolan.org/vlc)

- [mpv player](https://mpv.io/)

### Video platform

- [PeerTube](https://joinpeertube.org/) (Alternative to YouTube) : PeerTube is a freedom-respecting, decentralized and federated video platform developed as an alternative to other platforms that centralize our data and attention, such as YouTube, Dailymotion or Vimeo. Check [this link](https://github.com/Chocobozzz/PeerTube#--------) for detailed description. [Here is a video](https://framatube.org/videos/watch/217eefeb-883d-45be-b7fc-a788ad8507d3) explaining what PeerTube is and how it works.

### Livestreaming

- [Owncast](http://owncast.online/) : It is an alternative to Twitch. Description [here](https://github.com/owncast/owncast#about-the-project). 

- [PeerTube](https://joinpeertube.org/) : PeerTube is a freedom-respecting, decentralized and federated video platform developed as an alternative to other platforms that centralize our data and attention, such as YouTube, Dailymotion or Vimeo. Check [this link](https://github.com/Chocobozzz/PeerTube#--------) for detailed description. [Here is a video](https://framatube.org/videos/watch/217eefeb-883d-45be-b7fc-a788ad8507d3) explaining what PeerTube is and how it works.

### Social Network

- [Mastodon](https://github.com/mastodon/mastodon#) (Alternative to Twitter)  : Mastodon is a free social network server based on ActivityPub where users can follow friends and discover new ones. On Mastodon, users can publish anything they want: links, pictures, text, video. All Mastodon servers are interoperable as a federated network (users on one server can seamlessly communicate with users from another one, including non-Mastodon software that implements ActivityPub)!


- [Pleroma](https://git.pleroma.social/pleroma/pleroma/#about) (Alternative to Twitter) : Pleroma is a microblogging server software that can federate (= exchange messages with) other servers that support ActivityPub. What that means is that you can host a server for yourself or your friends and stay in control of your online identity, but still exchange messages with people on larger servers. Pleroma will federate with all servers that implement ActivityPub, like Friendica, GNU Social, Hubzilla, Mastodon, Misskey, Peertube, and Pixelfed.

- [PixelFed](https://pixelfed.org/) (Alternative of Instagram) : A free and ethical photo sharing platform, powered by ActivityPub federation. List of instances [here](https://fediverse.party/en/pixelfed/).

- [Friendica](https://friendi.ca/) (Alternative of Facebook) : Friendica is a privacy-respecting alternative to Facebook. With Friendica,you can connect to anyone on Friendica, Mastodon, Diaspora, GnuSocial, Pleroma, or Hubzilla, regardless where each user profile is hosted. Description [here](https://github.com/friendica/friendica#friendica-social-communications-server).

- [Diaspora](https://diasporafoundation.org) : Diaspora is a privacy-aware, distributed, free social network.

Check out the diaspora pods [poddery.com](https://poddery.com/) and [diasp.in](https://diasp.in/).

[Tips](https://wiki.diasporafoundation.org/Choosing_a_pod) for finding a pod.

[List of diaspora servers](https://diaspora.podupti.me/) to sign up.

### Bookmarks sync 

- [xBrowserSync](https://www.xbrowsersync.org/) : xBrowserSync is a completely freedom-respecting tool for syncing your bookmarks and browser data between your desktop browsers. xBrowserSync respects your privacy and gives you complete anonymity — no sign up is required and no personal data is ever collected. It is also completely secure; your data is encrypted client-side with military grade encryption, Even if someone intercepted your data, only you can decrypt it.

List of public instances of xBrowserSync is [here](https://www.xbrowsersync.org/).

### Note-Making

These software are alternatives to Evernote, Google Keep, or Microsoft OneNote. 

- [Joplin](https://joplinapp.org/) : Joplin is a free/swatantra and fully-featured note-taking and to-do application which can handle a large number of markdown notes organized into notebooks and tags. It offers end-to-end encryption and can sync through Nextcloud, Dropbox, and more. It also offers easy import from Evernote and plain-text notes. 

Check the description [here](https://github.com/laurent22/joplin).

### File Syncing

- [Syncthing](https://syncthing.net/) (Alternative to OneDrive, Google Drive, DropBox ): Syncthing is a continuous file synchronization program. It synchronizes files between two or more computers in real time, safely protected from prying eyes.

### Password Manager

Password Manager is a program to generate, store and manage passwords. 

Note: I haven't used any of these password managers. If you are looking for a password manager, then you can check out these freedom-respecting password managers and use the one that suits your needs.

These password managers serve as a alternatives of the nonfree/proprietary password managers, for example, 1Password, LastPass, Roboform, iCloud Keychain. 

- [KeePassXC](https://keepassxc.org/) : KeePassXC is a freedom-respecting password manager that stores password locally in your computer and all the passwords are in a single file that makes it easy to backup.  It saves many different types of information, such as usernames, passwords, URLs, attachments, and notes in an offline, encrypted file that can be stored in any location. If you want Bitwarden-like sync, you can put the DB in Nexcloud/Syncthing etc.
Description [here](https://github.com/keepassxreboot/keepassxc#-keepassxc).

- [pass](https://www.passwordstore.org/) : A command line program for password manager. With pass, each password lives inside of a gpg encrypted file whose filename is the title of the website or resource that requires the password.

The downside is that it creates a new file for every password and therefore harder to backup and there is no Android app.

- [Bitwarden](https://bitwarden.com/) : Very convenient password manager. The passwords are synced between all of your devices. If you don't want to use the Bitwarden server to store your passwords, you can easily host your own Bitwarden server.

### Scanner App

A scanner app is usually used in mobile phones to take photos of documents to convert it into an image or a PDF.  

- [Open Note Scanner](https://f-droid.org/en/packages/com.todobom.opennotescanner/) : With Open Note Scanner you can capture documents, handwritten notes, arts,
shopping receipts, etcetera. It will automatically adjust the image aspect, contrast and save it. It will allow browsing, view and share the scanned documents.

<br>

I think I missed a lot of categories and I will add them later. In the meanwhile, you can also send your suggestions.


