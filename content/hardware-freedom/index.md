---
title: "Hardware Freedom"
---
This file lists resources/articles on hardware that respects your freedom.

- [Libre Tech Shop](https://libretech.shop/) : Libre Tech Shop is an Indian based shop which sells hardware that respect your freedom to learn, hack, extend and do things yourself. The laptops, called Liberated Computer, are capable of running fully [free software](https://www.gnu.org/philosophy/free-software-even-more-important.html). They sell other ethical hardware products as well. 

- [Purism](https://puri.sm/) : Purism is a USA-based startup which sells free software powered laptops and mobile phones which respect your privacy. 

- [System 76](https://system76.com/) : System 76, based in USA, sells hardware which can run exclusively on free software. 
