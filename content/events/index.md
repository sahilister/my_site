#### Upcoming Events and Ongoing Campaigns

- Join me in educating teachers about the importance of free software and helping the educational institutes to switch to free software. Please [write to me](https://ravidwivedi.in/contact) if you are interested. 

For details, check out [Educational Institutes should use exclusively freedom-respecting software](https://fsf.org.in/article/education).


- Delete Outlook : Outlook is an email service by Microsoft. It [blocked incoming mails from Disroot servers](https://disroot.org/de/blog/microsoft_hostility) for no reason. Outlook also blocks emails from FSCI servers. I think it is a standard practice for Outlook. 

If you are an Outlook user, try to take steps to leave Outlook and switch to a new mail provider.

- [FSCI's Jitsi Meet Crowdfunding Campaign](https://fund.fsci.in) : Jitsi Meet is a [freedom-respecting](https://www.gnu.org/philosophy/free-sw.html), secure, fully-featured video conferencing software that can be used over mobile devices and desktop. [Free Software Community of India(FSCI)](https://fsci.in) is running a Jitsi service at [meet.fsci.in](https://meet.fsci.in), funded by public donations, to promote free software and privacy. 

Please [donate](https://fund.fsci.in/) to support the campaign. 

[Why Jitsi?](https://fsf.org.in/article/better-than-zoom)

-  [Software Freedom Camp 2021(Diversity Edition)](https://camp.fsci.in) (Schedule is yet to be announced) : FSCI is planning to conduct a camp to promote [free software](https://www.gnu.org/philosophy/free-sw.html). This year (in 2021) the camp focuses on reaching out to underrepresented people in the technical industry in India. 

If you would like to help in organizing the camp or translating the [camp website](camp.fsci.in) into your local language, please [write to me](https://ravidwivedi.in/contact) or send an email to camp at fsci.in .

I am a part of the organizing team of this camp. Please help us in reaching out to interested people by spreading the word. The official Fediverse page of the camp is [here](https://fsmi.social/@sfcamp). 

- [DebConf21](https://debconf21.debconf.org/) (August 22 to August 29, 2021): Check the description of DebConf [here](https://debconf21.debconf.org/about/debconf/).

- [Degoogle your life](https://github.com/Degoogle-your-life/Why-you-should-avoid-Google-AMP) : Take steps to remove or, at least, minimize your dependence on the surveillance company Google. I will post some resources on this topic in the future.
